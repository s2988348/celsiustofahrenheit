package nl.utwente.di.celToFah;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class CelToFah extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private FahrenheitCalc fahrenheitCalc;
	
    public void init() throws ServletException {
        fahrenheitCalc = new FahrenheitCalc();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius To Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius in degrees: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit in degrees: " +
                   fahrenheitCalc.getFahrenheit(request.getParameter("celsius")) +
                "</BODY></HTML>");
  }
}
