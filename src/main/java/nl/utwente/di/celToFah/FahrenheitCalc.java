package nl.utwente.di.celToFah;

public class FahrenheitCalc {
    public double getFahrenheit(String celsius) {
        double celDegree = Double.parseDouble(celsius);
        return ((celDegree *9)/5) + 32;
    }
}
