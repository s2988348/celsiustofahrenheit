package nl.utwente.di.celToFah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/*** Test the Quoter */
public class TestFahrenheitCalc {

    @Test
    public void TestFahrenheit() {
        FahrenheitCalc fah = new FahrenheitCalc();
        double fahrenheit = fah.getFahrenheit("50");
        Assertions.assertEquals(122.0, fahrenheit, 0.0,  "Fahrenheit in degrees: ");
    }

}
